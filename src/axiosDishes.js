import axios from "axios";

const axiosDishes = axios.create({
  baseURL: "https://alimbekov-224244-default-rtdb.firebaseio.com/",
});

export default axiosDishes;
