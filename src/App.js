import { Route, Switch } from "react-router-dom";
import "./App.css";
import Menu from "./Containers/Menu/Menu";

const App = () => (
  <div className="Container">
    <Switch>
      <Route path="/" exact component={Menu} />
    </Switch>
  </div>
);

export default App;
