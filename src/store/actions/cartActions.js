import axiosDishes from "../../axiosDishes";

export const ADD = "ADD";
export const REMOVE = "REMOVE";
export const SHOW = "SHOW";
export const HIDE = "HIDE";
export const INIT_CART = "INIT_CART";

export const AddDishes = (dishesName) => ({ type: ADD, dishesName });
export const RemoveDishes = (dishesName) => ({ type: REMOVE, dishesName });
export const showModal = () => ({ type: SHOW });
export const hideModal = () => ({ type: HIDE });
export const initCart = () => ({ type: INIT_CART });

export const POST_CART_REQUEST = "POST_CART_REQUEST";
export const POST_CART_SUCCESS = "POST_CART_SUCCESS";
export const POST_CART_FAILURE = "POST_CART_FAILURE";

export const postCartRequest = () => ({ type: POST_CART_REQUEST });
export const postCartSuccess = () => ({ type: POST_CART_SUCCESS });
export const postCartFailure = (error) => ({ type: POST_CART_FAILURE, error });

export const createOrder = (cart) => {
  return async (dispatch) => {
    try {
      dispatch(postCartRequest());
      await axiosDishes.post("carts.json", cart);
      dispatch(postCartSuccess());
    } catch (e) {
      dispatch(postCartFailure(e));
    }
  };
};
