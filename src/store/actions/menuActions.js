import axiosDishes from "../../axiosDishes";

export const FETCH_DISHES_REQUEST = "FETCH_DISHES_REQUEST";
export const FETCH_DISHES_SUCCESS = "FETCH_DISHES_SUCCESS";
export const FETCH_DISHES_FAILURE = "FETCH_DISHES_FAILURE";

export const fetchDishesRequest = () => ({ type: FETCH_DISHES_REQUEST });
export const fetchDishesSuccess = (dishes) => ({
  type: FETCH_DISHES_SUCCESS,
  dishes,
});
export const fetchDishesFailure = (error) => ({
  type: FETCH_DISHES_FAILURE,
  error,
});

export const fetchDishes = () => {
  return async (dispatch) => {
    try {
      dispatch(fetchDishesRequest());
      const response = await axiosDishes.get("dishes.json");
      const dishes = Object.keys(response.data).map((disheName) => ({
        ...response.data[disheName],
        disheName,
      }));
      dispatch(fetchDishesSuccess(dishes));
    } catch (e) {
      dispatch(fetchDishesFailure(e));
    }
  };
};
