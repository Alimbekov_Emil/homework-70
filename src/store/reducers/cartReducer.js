import { CART_PRICES } from "../../constans";
import {
  ADD,
  HIDE,
  INIT_CART,
  POST_CART_FAILURE,
  POST_CART_REQUEST,
  POST_CART_SUCCESS,
  REMOVE,
  SHOW,
} from "../actions/cartActions";

const initialState = {
  cart: {
    bread: 0,
    pilaf: 0,
    salad: 0,
  },
  modal: false,
  sum: 150,
  error: null,
  loading: false,
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case INIT_CART:
      return { ...initialState };
    case ADD:
      return {
        ...state,
        cart: {
          ...state.cart,
          [action.dishesName]: state.cart[action.dishesName] + 1,
        },
        sum: state.sum + CART_PRICES[action.dishesName],
      };
    case REMOVE:
      return {
        ...state,
        cart: {
          ...state.cart,
          [action.dishesName]: state.cart[action.dishesName] - 1,
        },
        sum: state.sum - CART_PRICES[action.dishesName],
      };
    case SHOW:
      return { ...state, modal: true };
    case HIDE:
      return { ...state, modal: false };
    case POST_CART_REQUEST:
      return { ...state, loading: true };
    case POST_CART_SUCCESS:
      return { ...state, loading: false };
    case POST_CART_FAILURE:
      return { ...state, loading: false, erorr: action.error };
    default:
      return { ...state };
  }
};

export default cartReducer;
