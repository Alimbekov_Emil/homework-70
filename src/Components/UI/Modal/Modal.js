import React, { useState } from "react";
import "./Modal.css";
import Backdrop from "../Backdrop/Backdrop";
import { useDispatch, useSelector } from "react-redux";
import Spinner from "../Spinner/Spinner";
import {
  createOrder,
  hideModal,
  initCart,
} from "../../../store/actions/cartActions";

const Modal = (props) => {
  const dispatch = useDispatch();

  const [users, setUsers] = useState({
    name: "",
    phone: "",
    address: "",
  });

  const cart = useSelector((state) => state.cart.cart);
  const loading = useSelector((state) => state.cart.loading);

  const orderHandler = (e) => {
    e.preventDefault();

    const order = {
      cart,
      users: { ...users },
    };

    dispatch(createOrder(order));

    setUsers({ name: "", phone: "", address: "" });

    dispatch(hideModal());

    dispatch(initCart());
  };

  const userDataChanged = (event) => {
    const { name, value } = event.target;

    setUsers((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  return (
    <>
      <Backdrop show={props.show} onClick={props.closed} />
      <form
        onSubmit={orderHandler}
        className="Modal"
        style={{
          transform: props.show ? "translateY(0)" : "translateY(-100vh)",
          opacity: props.show ? "1" : "0",
        }}
      >
        {loading ? <Spinner /> : null}

        <input
          name="name"
          placeholder="Name"
          className="Input"
          onChange={userDataChanged}
          value={users.name}
        />
        <input
          name="phone"
          type="number"
          placeholder="Your Phone"
          className="Input"
          onChange={userDataChanged}
          value={users.phone}
        />
        <input
          name="address"
          placeholder="Your Address"
          className="Input"
          onChange={userDataChanged}
          value={users.address}
        />
        <button type="submit">Create Order</button>
        <button type="button" onClick={props.closed}>
          Cancel
        </button>
      </form>
    </>
  );
};

export default Modal;
