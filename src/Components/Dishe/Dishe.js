import React from "react";
import "./Dishe.css";

const Dishe = (props) => {
  return (
    <div className="Dishe">
      <img src={props.image} />
      <div>
        <p>{props.name}</p>
        <p>KGS: {props.price}</p>
      </div>
      <button onClick={() => props.add(props.id)}>Add to cart</button>
    </div>
  );
};

export default Dishe;
