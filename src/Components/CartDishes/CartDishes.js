import React from "react";
import "./CartDishes.css";

const CartDishes = ({ name, quantity, add, remove }) => {
  return (
    <div className="CartDishes">
      <p>
        {name} x {quantity}
      </p>
      <button onClick={() => add(name)}>+</button>
      <button onClick={() => remove(name)}>-</button>
    </div>
  );
};

export default CartDishes;
