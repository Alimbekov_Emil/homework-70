import React from "react";
import { useDispatch, useSelector } from "react-redux";
import CartDishes from "../../Components/CartDishes/CartDishes";
import Modal from "../../Components/UI/Modal/Modal";
import {
  AddDishes,
  hideModal,
  RemoveDishes,
  showModal,
} from "../../store/actions/cartActions";
import "./Cart.css";

const Cart = () => {
  const dispatch = useDispatch();
  const sum = useSelector((state) => state.cart.sum);
  const dishesOrders = useSelector((state) => state.cart.cart);
  const modal = useSelector((state) => state.cart.modal);

  const addDisheHandler = (disheName) => {
    dispatch(AddDishes(disheName));
  };
  const removeDisheHandler = (disheName) => {
    dispatch(RemoveDishes(disheName));
  };
  const purchaseHandler = () => {
    dispatch(showModal());
  };
  const purchaseCancelHandler = () => {
    dispatch(hideModal());
  };
  return (
    <div className="Cart">
      <h1>Cart</h1>
      {Object.keys(dishesOrders).map((dishe) => {
        return dishesOrders[dishe] > 0 ? (
          <CartDishes
            key={dishe}
            name={dishe}
            quantity={dishesOrders[dishe]}
            add={addDisheHandler}
            remove={removeDisheHandler}
          />
        ) : null;
      })}
      <Modal show={modal} closed={purchaseCancelHandler} />
      <p> delivery 150 KGS</p>
      <p>Total : {sum}</p>
      {sum > 150 ? <button onClick={purchaseHandler}>Place Order</button> : null}
    </div>
  );
};

export default Cart;
