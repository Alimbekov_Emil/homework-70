import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Dishe from "../../Components/Dishe/Dishe";
import Spinner from "../../Components/UI/Spinner/Spinner";
import { AddDishes } from "../../store/actions/cartActions";
import { fetchDishes } from "../../store/actions/menuActions";
import Cart from "../Cart/Cart";
import "./Menu.css";

const Menu = () => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state.menu);

  useEffect(() => {
    dispatch(fetchDishes());
  }, [dispatch]);

  const addDisheHandler = (disheName) => {
    dispatch(AddDishes(disheName));
  };

  return (
    <div className="Menu">
      <div className="Dishes">
        {state.loading ? <Spinner /> : null}
        {state.dishes.map((dishe) => {
          return (
            <Dishe
              key={dishe.name}
              id={dishe.name}
              name={dishe.disheName}
              price={dishe.price}
              image={dishe.image}
              add={addDisheHandler}
            />
          );
        })}
      </div>
      <Cart />
    </div>
  );
};

export default Menu;
